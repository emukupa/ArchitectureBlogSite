import React, { Component } from 'react';
//import logo from '../../logo.svg';
import firebase from '../../firebase.js';
import "./Sketch.css";

class Sketch extends Component {
  constructor() {
    super();
    this.state = {
      sketches: []
    }
  }

  componentDidMount() {
    const sketchesRef = firebase.database().ref('sketches');
    sketchesRef.on('value', (snapshot) => {
      let sketches = snapshot.val();
      let newState = [];
      for (let sketch in sketches) {
        newState.push({
          id: sketch,
          imgUrl: sketches[sketch].imgUrl,
          user: sketches[sketch].user
        });
      }
      this.setState({
        sketches: newState
      });
    });
  }

  render() {
    return (
      <div className='app'>
        <header>
          <div className='wrapper'>
            <h1>Sketch</h1>
          </div>
        </header>
            <div className='container'>
              <section className='display-sketch'>
                  <ul>
                    {this.state.sketches.map((sketch) => {
                      return (
                        <li className="card" key={sketch.id}>
                          <img className="card-img-top" src="http://i.imgur.com/8ozWbMm.jpg, http://i.imgur.com/8ozWbMm.jpg" alt=""/>
                          <div className="card-body">
                            <h5 className="card-title">Written by: {sketch.user}</h5>
                            <p className="card-text">{sketch.imgUrl}</p>
                          </div>
                        </li>
                      )
                    })}
                  </ul>
              </section>
            </div>
      </div>
    );
  }
}
export default Sketch;
