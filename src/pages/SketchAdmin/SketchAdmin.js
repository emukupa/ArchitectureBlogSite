import React, { Component } from 'react';
//import logo from '../../logo.svg';
import firebase, { auth, provider } from '../../firebase.js';
import "./SketchAdmin.css";

class SketchAdmin extends Component {
  constructor() {
    super();
    this.state = {
      username: '',
      currentSketch: '',
      sketches: [],
      user: null

    }
      this.login = this.login.bind(this);
      this.logout = this.logout.bind(this);
      this.handleChange = this.handleChange.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);
  }

  logout() {
    auth.signOut()
      .then(() => {
        this.setState({
          user: null
        });
      });
  }

  login() {
    auth.signInWithPopup(provider)
      .then((result) => {
        const user = result.user;
        this.setState({
          user
        });
      });
  }

  handleChange(e) {
    this.setState({
      [e.target.name]: e.target.value
    });
  }

  handleSubmit(e) {
    e.preventDefault();
    const sketchesRef = firebase.database().ref('sketches');
    const sketch = {
      imgUrl: this.state.currentSketch,
      user: this.state.user.email || this.state.user.displayName
    }
    sketchesRef.push(sketch);
    this.setState({
      currentSketch: '',
      username: ''
    });
  }

  componentDidMount() {
    auth.onAuthStateChanged((user) => {
      if (user) {
        this.setState({ user });
      }
    });
    const sketchesRef = firebase.database().ref('sketches');
    sketchesRef.on('value', (snapshot) => {
      let sketches = snapshot.val();
      let newState = [];
      for (let sketch in sketches) {
        newState.push({
          id: sketch,
          imgUrl: sketches[sketch].imgUrl,
          user: sketches[sketch].user
        });
      }
      this.setState({
        sketches: newState
      });
    });
  }

  removeSketch(sketchId) {
    const sketchRef = firebase.database().ref(`/sketches/${sketchId}`);
    sketchRef.remove();
  }

  render() {
    return (
      <div className='app'>
        <header>
          <div className='wrapper'>
            <h1>SketchAdmin</h1>
            {this.state.user != null && this.state.user.email === "caruzzo@gmail.com" ?
            <div>
              <button className="btn btn-primary" onClick={this.logout}>Log Out</button>
              <span className='user-profile'>
                <img src={this.state.user.photoURL} alt=""/>
              </span>
              </div>
            :
              <button className="btn btn-primary" onClick={this.login}>Log In</button>
            }
          </div>
        </header>
        {this.state.user != null && this.state.user.email === "caruzzo@gmail.com" ?
          <div>
            <div className='container'>
              <section className='display-sketch'>
                  <ul>
                    {this.state.sketches.map((sketch) => {
                      return (
                        <li className="card" key={sketch.id}>
                          <img className="card-img-top" src="http://i.imgur.com/8ozWbMm.jpg, http://i.imgur.com/8ozWbMm.jpg" alt=""/>
                          <div className="card-body">
                            <h5 className="card-title">Written by: {this.state.user.displayName}
                              {sketch.user === this.state.user.email || sketch.user === this.state.user.displayName ?
                                <button className="btn btn-primary" onClick={() => this.removeSketch(sketch.id)}>Remove Sketch</button> : null}</h5>
                                <p className="card-text">{sketch.imgUrl}</p>
                          </div>
                        </li>
                      )
                    })}
                  </ul>
              </section>
              <section className='add-sketch'>
                <form onSubmit={this.handleSubmit}>
                  <input type="text" name="username" placeholder="What's your name?" defaultValue={this.state.user.displayName || this.state.user.email} />
                  <input type="textarea" name="currentSketch" placeholder="Write sketch here." onChange={this.handleChange} value={this.state.currentSketch} />
                  <input type="file" className="form-control-file" id="exampleFormControlFile1"/>
                  <button className="btn btn-primary">Add Sketch</button>
                </form>
              </section>
            </div>

          </div>
          :
          <div className='wrapper'>
            <p>You must be logged in to edit sketches.</p>
          </div>
        }
      </div>
    );
  }
}
export default SketchAdmin;
