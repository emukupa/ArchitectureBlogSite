import React, { Component } from 'react';
//import logo from '../../logo.svg';
import firebase from '../../firebase.js';
import "./Blog.css";

class Blog extends Component {
  constructor() {
    super();
    this.state = {
      blogs: []
    }
  }

  componentDidMount() {
    const blogsRef = firebase.database().ref('blogs');
    blogsRef.on('value', (snapshot) => {
      let blogs = snapshot.val();
      let newState = [];
      for (let blog in blogs) {
        newState.push({
          id: blog,
          post: blogs[blog].post,
          user: blogs[blog].user
        });
      }
      this.setState({
        blogs: newState
      });
    });
  }

  render() {
    return (
      <div className='app'>
        <header>
          <div className='wrapper'>
            <h1>Blog</h1>
          </div>
        </header>
          <div>
            <div className='container'>
              <section className='display-blog'>
                  <ul>
                    {this.state.blogs.map((blog) => {
                      return (
                        <li className="card" key={blog.id}>
                        <div className="card-body">
                          <h5 className="card-post">Written by: {blog.user}</h5>
                          <p className="card-text">{blog.post}</p>
                        </div>
                        </li>
                      )
                    })}
                  </ul>
              </section>
            </div>
          </div>
      </div>
    );
  }
}
export default Blog;
