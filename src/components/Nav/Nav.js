import React from "react";
import { Link } from "react-router-dom";
import "./Nav.css";

const Nav = () => (
  <nav className="navbar navbar-default">
    <div className="container-fluid">
      <div className="navbar-header">
        <Link className="navbar-brand" to="/">
          Home
        </Link>
        <Link className="navbar-brand" to="/blog">
          Blog
        </Link>
        <Link className="navbar-brand" to="/blogAdmin">
          BlogAdmin
        </Link>
        <Link className="navbar-brand" to="/sketch">
          Sketch
        </Link>
        <Link className="navbar-brand" to="/sketchAdmin">
          SketchAdmin
        </Link>
      </div>
    </div>
  </nav>
);

export default Nav;
