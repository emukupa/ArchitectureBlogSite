import React, { Component } from 'react';
import './App.css';
import BlogAdmin from "./pages/BlogAdmin";
import Blog from "./pages/Blog";
import SketchAdmin from "./pages/SketchAdmin";
import Sketch from "./pages/Sketch";
import Nav from "./components/Nav";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

class App extends Component {

  render() {
    return (
      <Router>
        <div>
          <Nav />
          <Switch>
            <Route exact path="/blog" component={Blog} />
            <Route exact path="/blogAdmin" component={BlogAdmin} />
            <Route exact path="/sketch" component={Sketch} />
            <Route exact path="/sketchAdmin" component={SketchAdmin} />
          </Switch>
        </div>
      </Router>
    );
  }
}
export default App;
