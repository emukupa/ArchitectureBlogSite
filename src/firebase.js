import firebase from 'firebase'

// Initialize Firebase
var config = {
  apiKey: "AIzaSyDskPhlXSG9EUsDZBWH1iogEsYN6H4tI0g",
  authDomain: "blog-ab7d3.firebaseapp.com",
  databaseURL: "https://blog-ab7d3.firebaseio.com",
  projectId: "blog-ab7d3",
  storageBucket: "blog-ab7d3.appspot.com",
  messagingSenderId: "125843946930"
};
firebase.initializeApp(config);
export const provider = new firebase.auth.GoogleAuthProvider();
export const auth = firebase.auth();
export default firebase;
