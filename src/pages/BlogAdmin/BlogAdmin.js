import React, { Component } from 'react';
//import logo from '../../logo.svg';
import firebase, { auth, provider } from '../../firebase.js';
import "./BlogAdmin.css";

class BlogAdmin extends Component {
  constructor() {
    super();
    this.state = {
      username: '',
      currentBlog: '',
      blogs: [],
      user: null

    }
      this.login = this.login.bind(this);
      this.logout = this.logout.bind(this);
      this.handleChange = this.handleChange.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);
  }

  logout() {
    auth.signOut()
      .then(() => {
        this.setState({
          user: null
        });
      });
  }

  login() {
    auth.signInWithPopup(provider)
      .then((result) => {
        const user = result.user;
        this.setState({
          user
        });
      });
  }

  handleChange(e) {
    this.setState({
      [e.target.name]: e.target.value
    });
  }

  handleSubmit(e) {
    e.preventDefault();
    const blogsRef = firebase.database().ref('blogs');
    const blog = {
      post: this.state.currentBlog,
      user: this.state.user.email || this.state.user.displayName
    }
    blogsRef.push(blog);
    this.setState({
      currentBlog: '',
      username: ''
    });
  }

  componentDidMount() {
    auth.onAuthStateChanged((user) => {
      if (user) {
        this.setState({ user });
      }
    });
    const blogsRef = firebase.database().ref('blogs');
    blogsRef.on('value', (snapshot) => {
      let blogs = snapshot.val();
      let newState = [];
      for (let blog in blogs) {
        newState.push({
          id: blog,
          post: blogs[blog].post,
          user: blogs[blog].user
        });
      }
      this.setState({
        blogs: newState
      });
    });
  }

  removeBlog(blogId) {
    const blogRef = firebase.database().ref(`/blogs/${blogId}`);
    blogRef.remove();
  }

  render() {
    return (
      <div className='app'>
        <header>
          <div className='wrapper'>
            <h1>BlogAdmin</h1>
            {this.state.user != null && this.state.user.email === "caruzzo@gmail.com" ?
            <div>
              <button className="btn btn-primary" onClick={this.logout}>Log Out</button>
              <span className='user-profile'>
                <img src={this.state.user.photoURL} alt=""/>
              </span>
              </div>
            :
              <button className="btn btn-primary" onClick={this.login}>Log In</button>
            }
          </div>
        </header>
        {this.state.user != null && this.state.user.email === "caruzzo@gmail.com" ?
          <div>
            <div className='container'>
              <section className='display-blog'>
                  <ul>
                    {this.state.blogs.map((blog) => {
                      return (

                        <li className="card" key={blog.id}>
                        <div className="card-body">
                          <h5 className="card-post">Written by: {this.state.user.displayName}
                            {blog.user === this.state.user.email || blog.user === this.state.user.displayName ?
                              <button className="btn btn-primary" onClick={() => this.removeBlog(blog.id)}>Remove Blog</button> : null}</h5>
                          <p className="card-text">{blog.post}</p>
                          </div>
                        </li>
                      )
                    })}
                  </ul>
              </section>
              <section className='add-blog'>
                <form onSubmit={this.handleSubmit}>
                  <input type="text" name="username" placeholder="What's your name?" defaultValue={this.state.user.email || this.state.user.displayName} />
                  <textarea name="currentBlog" placeholder="Write blog here." onChange={this.handleChange} value={this.state.currentBlog} />
                  <button className="btn btn-primary">Add Blog</button>
                </form>
              </section>
            </div>

          </div>
          :
          <div className='wrapper'>
            <p>You must be logged in to edit blogs.</p>
          </div>
        }
      </div>
    );
  }
}
export default BlogAdmin;
